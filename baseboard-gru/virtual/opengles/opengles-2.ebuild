# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

DESCRIPTION="Virtual for OpenGLES implementations"

SLOT="0"
KEYWORDS="-* arm64 arm"
IUSE="video_cards_llvmpipe"

DEPEND="
	video_cards_llvmpipe? ( media-libs/mesa[-vulkan,llvm,video_cards_llvmpipe] )
	!video_cards_llvmpipe? ( media-libs/mali-drivers-bin )
	x11-drivers/opengles-headers
"
RDEPEND="${DEPEND}"
